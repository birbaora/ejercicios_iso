#!/bin/bash
#este script pide un título y unas fotos y renombra las fotos con el título añadiendo la fecha de las fotos

fotosElegidas=$( zenity --file-selection --multiple --title="Fotos jpg elegidas" --filename=$PWD)
if [ $? -ne 0 ] ; then
	echo "No se ha podido elegir . Saliendo"
	exit -1;
fi
#dirElegidas="/home/nacho/Descargas/Fotos/hackaton_Divendres"


fotosElegidas=$( echo $fotosElegidas | tr '|' ' ' )

rutaFotos=$( echo $fotosElegidas | cut -d" " -f 1 | xargs dirname )
echo "RUTA = $rutaFotos"

dir=$rutaFotos
dir="${dir%/}"             # strip trailing slash (if any)
subdir="${dir##*/}"


titulo=$( zenity --entry \
--title="Título" \
--text="Escriba el título de las fotos" \
--entry-text "$subdir"
)

echo "El título será $titulo"
for file in $fotosElegidas ; do
	echo -n "$file  "
	exiv2 --verbose --Force -t --rename ${titulo}_%H%M-%d%m%Y $file 
done

