#!/bin/bash
dirElegidas=$( zenity --file-selection --directory --title="Carpeta de fotos jpg elegidas" )
if [ $? -ne 0 ] ; then
	echo "No se ha podido elegir una carpeta. Saliendo"
	exit -1;
fi
#dirElegidas="/home/nacho/Descargas/Fotos/hackaton_Divendres"
echo $dirElegidas

#dirProcesadas=$( zenity --file-selection --directory --title="Carpeta de fotos reprocesadas" )
#dirProcesadas="/home/nacho/Descargas/Fotos/hackaton_Divendres/raw/darktable_exported"
echo $dirProcesadas

dirCr=$( zenity --file-selection --directory --title="Carpeta de fotos RAW" --filename=${dirElegidas} )


#dirCr="/home/nacho/Descargas/Fotos/hackaton_Divendres/raw"
dirDestino="${dirElegidas}/rawElegidas"
mkdir $dirDestino
rm ${dirDestino}/noCopiadas*
for file in $( ls ${dirElegidas}/*.JPG ) ; do
	echo -n "$file  "
	name=$(basename  -s ".jpg" -s ".JPG" $file  )
	echo $name 
	
#	cp ${dirProcesadas}/${name}.[Jj][Pp][Gg] ${dirElegidas}
	cp ${dirCr}/${name}.CR2 ${dirDestino} 2>> ${dirDestino}/noCopiadasCR2
	cp ${dirCr}/${name}.ARW ${dirDestino} 2>> ${dirDestino}/noCopiadasARW
	cp ${dirCr}/${name}.RW2 ${dirDestino} 2>> ${dirDestino}/noCopiadasRW2
	cp ${dirCr}/${name}.PEF ${dirDestino} 2>> ${dirDestino}/noCopiadasPEF
done

lines=$( wc -l ${dirDestino}/noCopiadasCR2 | cut -d" "  -f1 )
if [ -e ${dirDestino}/noCopiadasCR2 ] && [ $lines -gt 0 ]  ; then
	echo "Hay archivos CR2 no copiados"
fi 

lines=$( wc -l ${dirDestino}/noCopiadasARW | cut -d" "  -f1 )
if [ -e ${dirDestino}/noCopiadasARW ] && [ $lines -gt 0 ]  ; then
	echo "Hay archivos ARW no copiados"
fi 

lines=$( wc -l ${dirDestino}/noCopiadasRW2 | cut -d" "  -f1 )
if [ -e ${dirDestino}/noCopiadasRW2 ] && [ $lines -gt 0 ]  ; then
	echo "Hay archivos RW2 no copiados"
fi 

lines=$( wc -l ${dirDestino}/noCopiadasPEF | cut -d" "  -f1 )
if [ -e ${dirDestino}/noCopiadasPEF ] && [ $lines -gt 0 ]  ; then
	echo "Hay archivos PEF no copiados"
fi


