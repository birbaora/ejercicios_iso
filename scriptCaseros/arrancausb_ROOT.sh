#!/bin/bash

for i in /sys/bus/pci/drivers/[eo]hci-pci/*:*; do
  [ -e "$i" ] || continue
  echo "${i##*/}" > "${i%/*}/unbind"
  echo "${i##*/}" > "${i%/*}/bind"
  echo "${i##*/}"
done

