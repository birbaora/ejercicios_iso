#!/bin/bash

if [ $# -ne 0 ] ; then
	dirElegidas="$1"
	if ! [ -d "$1" ] ; then
		echo "Error: $1 no es un directorio" >&2
		exit;
	fi
else
	dirElegidas=$( zenity --file-selection --directory --title="Carpeta de fotos jpg elegidas" )
fi
#dirElegidas="/home/nacho/Descargas/Fotos/hackaton_Divendres"
#echo "carpeta = $dirElegidas"
cd $dirElegidas

for file in $( ls ) ; do
	if [ -d "${file}" ] ; then
		$0 ${file}
	fi
done



for file in $( ls *.jpg 2>/dev/null) ; do
	name=$(basename  -s ".jpg"  $file  )
	nameAlt="${name}.JPG"
	if [ -f "${nameAlt}" ] ; then
		echo carpeta = ${dirElegidas} "${name}" and "${nameAlt}" encontrados
		mv "${name}.JPG" "${name}_original.jpg"
	fi
done
